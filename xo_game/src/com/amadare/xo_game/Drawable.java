package com.amadare.xo_game;

import com.badlogic.gdx.graphics.g2d.*;

public interface Drawable {
	public void Draw(SpriteBatch batch, BitmapFont font);
}
