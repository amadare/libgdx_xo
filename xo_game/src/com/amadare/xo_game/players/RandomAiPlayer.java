package com.amadare.xo_game.players;

import java.util.Random;

import com.amadare.xo_game.GameController;
import com.amadare.xo_game.GameModel;

public class RandomAiPlayer extends AiPlayer {

	public RandomAiPlayer(GameModel game, GameController controller, int mark) {
		super(game, controller, mark, "Random AI");
	}
	
	@Override
	public void MakeTurn() {
		super.MakeTurn();
		Random r = new Random();
		if (game.xIsActive == (mark==1)){
			int x=r.nextInt(3);
			int y=r.nextInt(3);
			
			while (game.map[x][y]!=0){
				x=r.nextInt(3);
				y=r.nextInt(3);
			}
			
			controller.place(x, y);
		}
	}

}
