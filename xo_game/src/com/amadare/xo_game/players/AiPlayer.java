package com.amadare.xo_game.players;

import com.amadare.xo_game.GameController;
import com.amadare.xo_game.GameModel;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;

public class AiPlayer extends Player {
	static public float Delay=0f;
	protected float elapsed;
	
	
	public AiPlayer(GameModel game, GameController controller, int mark, String name) {
		super(game, controller, mark, false, name);
	}
	
	@Override
	public void SetActive() {
		super.SetActive();
		elapsed=0;
	}
	
	@Override
	public void Update(float delta) {
		super.Update(delta);
		if (active){
			elapsed+=Gdx.graphics.getDeltaTime();
			if (elapsed>=Delay){
				MakeTurn();
			}
		}
	}
}
