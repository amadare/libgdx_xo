package com.amadare.xo_game.players;

import com.amadare.xo_game.*;

public class Player {
	protected final GameModel game;
	protected final GameController controller;
	public int mark;
	public final boolean IsHuman;
	public final String Name;
	protected boolean active;
	public int Wins;
	
	public Player(GameModel game, GameController controller, int mark, boolean human, String name){
		this.game=game;
		this.mark=mark;
		this.controller=controller;
		IsHuman=human;
		Name=name;
		active=false;
		Wins=0;
	}
	public void MakeTurn(){
		active=false;
	}
	public void setMark(int mark){
		this.mark=mark;		
	}
	public int MakeWin(){
		return ++Wins;
	}
	public void Update(float delta){}
	public void SetActive(){
		active=true;
	}
}
