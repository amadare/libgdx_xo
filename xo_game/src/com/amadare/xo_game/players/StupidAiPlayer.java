package com.amadare.xo_game.players;
import com.badlogic.gdx.utils.TimeUtils;

import com.amadare.xo_game.GameController;
import com.amadare.xo_game.GameModel;
import com.badlogic.gdx.Gdx;

public class StupidAiPlayer extends AiPlayer {

	public StupidAiPlayer(GameModel game, GameController controller, int mark) {
		super(game, controller, mark, "Random AI");
	}

	@Override
	public void MakeTurn() {
		super.MakeTurn();
		if (game.xIsActive == (mark==1)){
			main:
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					if (game.map[i][j]==0){
						controller.place(i, j);
						break main;
					}
		}
	}	
	
	@Override
	public void SetActive() {
		super.SetActive();	
	}
	
	@Override
	public void Update(float delta) {
		super.Update(delta);
	}

}
