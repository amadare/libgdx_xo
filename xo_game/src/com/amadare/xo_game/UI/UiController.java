package com.amadare.xo_game.UI;

import java.util.ArrayList;
import java.util.List;

import com.amadare.xo_game.*;
import com.amadare.xo_game.input.NeedInput;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class UiController implements NeedInput, Drawable, Updateable {
	public List<UiElement> Elements;
	private UiElement hoveredElement;
	public void AddElement(UiElement e){
		Elements.add(e);
	}
	public void RemoveElement(UiElement e){
		Elements.remove(e);
		if (hoveredElement==e)
			hoveredElement=null;
		e.dispose();
	}
	public UiController(){
		Elements = new ArrayList<UiElement>();
	}
	
	public void handleInput(float x, float y, boolean pressed){
		boolean hovered=false;
		for(UiElement e: Elements){
			if (e.Box.contains(x,y)){
				hovered=true;
				if (hoveredElement!=e){
					if (hoveredElement!=null)
						hoveredElement.Leave();
					hoveredElement=e;
					e.Hover();
				} else if (pressed){
					e.Press();
				}		
				
			}			
		}
		if (!hovered && hoveredElement!=null){
			hoveredElement.Leave();
			hoveredElement=null;
		}
	}
	
	public void Draw(SpriteBatch batch, BitmapFont font){
		for (UiElement e: Elements){
			if (e.Visible)
				e.Draw(batch, font);
		}
	}
	@Override
	public void Update(float delta) {
		for (UiElement e:Elements)
			e.Update(delta);
	}

}
