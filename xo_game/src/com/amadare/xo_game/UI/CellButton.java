package com.amadare.xo_game.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class CellButton extends UiElement {
	boolean hovered=false;
	Texture texture;
	
	public CellButton(Rectangle rect, Texture texture){
		this.Box=rect;
		this.texture=texture;
	}

	@Override
	public void Draw(SpriteBatch batch, BitmapFont font) {
		if (hovered && Active){
			batch.draw(texture,Box.x,Box.y,Box.width,Box.height);
		}
	}
	@Override
	public void Leave() {
		hovered=false;
	}
	
	@Override
	public void Hover() {
		hovered=true;
	}
}
