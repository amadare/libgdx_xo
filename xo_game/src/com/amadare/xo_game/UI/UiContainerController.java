package com.amadare.xo_game.UI;

import java.util.*;

import com.amadare.xo_game.Drawable;
import com.amadare.xo_game.input.NeedInput;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class UiContainerController implements NeedInput, Drawable {
	public Map<String,UiContainer> containers;	

	public UiContainerController(){
		containers = new HashMap<String,UiContainer>();
	}
	
	public void AddElement(String containerName, UiElement e){
		if (!containers.containsKey(containerName))
			containers.put(containerName, new UiContainer());
			
		containers.get(containerName).elements.add(e);
	}
	
	public void RemoveElement(String containerName, UiElement e){
		containers.get(containerName).RemoveElement(e);
	}
	
	public boolean ToggleContainerState(String name){
		if (!containers.containsKey(name))
			return false;
		containers.get(name).Toggle();
		return true;
	}
	
	public boolean SetContainerState(String name, boolean state){
		if (!containers.containsKey(name))
			return false;
		containers.get(name).Enabled=state;
		return true;
	}

	@Override
	public void handleInput(float x, float y, boolean pressed) {
		for (String s: containers.keySet()){
			UiContainer container = containers.get(s);
			if (containers.get(s).Enabled){
				boolean hovered=false;
				for(UiElement e: container.elements){
					if (e.Box.contains(x,y)){
						hovered=true;
						if (container.hoveredElement!=e){
							if (container.hoveredElement!=null)
								container.hoveredElement.Leave();
							container.hoveredElement=e;
							e.Hover();
						} else if (pressed){
							e.Press();
						}		
						
					}			
				}
				if (!hovered && container.hoveredElement!=null){
					container.hoveredElement.Leave();
					container.hoveredElement=null;
				}
			}
		}
	}

	@Override
	public void Draw(SpriteBatch batch, BitmapFont font) {
		for (String s: containers.keySet()){
			UiContainer container = containers.get(s);
			for(UiElement e: container.elements){
				if (e.Visible)
					e.Draw(batch, font);
			}
		}
	}

}
