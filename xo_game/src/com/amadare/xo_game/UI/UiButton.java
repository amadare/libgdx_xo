package com.amadare.xo_game.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class UiButton extends UiElement{
	String text;
	Texture texture,pressed;
	public int state;
	
	public UiButton(String text, Rectangle rect, Texture texture, Texture pressed){
		this.text=text;	
		state=0;
		this.Box=rect;
		this.texture=texture;
		this.pressed=pressed;
	}

	@Override
	public void Draw(SpriteBatch batch, BitmapFont font) {	
		batch.draw(state==0?texture:pressed,Box.x,Box.y,
				Box.width,Box.height);
		font.draw(batch, text, (Box.width-font.getBounds(text).width)/2+Box.x, 
				(Box.height-font.getBounds(text).height)/2+Box.y);	
	}
	
	public void Hover(){
		state=1;
	}
	
	public void Leave(){
		state=0;
	}
	
	public void Press(){
		state=0;
	}
}
