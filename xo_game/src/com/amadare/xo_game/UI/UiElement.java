package com.amadare.xo_game.UI;

import java.util.*;

import com.amadare.xo_game.UI.effects.UiEffect;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public abstract class UiElement{
	public Rectangle Box;
	public boolean Active=true, Visible=true;
	public Color color;
	
	public List<UiEffect> Effects=new ArrayList<UiEffect>();
	
	public abstract void Draw(SpriteBatch batch, BitmapFont font);
	public void dispose() {}
	public void Hover(){}
	public void Leave(){}
	public void Press(){}
	public void Update(float delta){
		for (UiEffect e: Effects){
			if (e.Enabled)
				e.Update(delta);
		}
	}
}