package com.amadare.xo_game.UI.effects;

import com.amadare.xo_game.Updateable;
import com.amadare.xo_game.UI.UiElement;

public class UiEffect implements Updateable{
	protected UiElement element;
	public boolean Enabled;
	
	public UiEffect(UiElement element){
		element.Effects.add(this);
		this.element=element;
		Enabled=true;
	}

	@Override
	public void Update(float delta) {
	}
	
	public void Remove(){
		element.Effects.remove(this);
	}
	
}
