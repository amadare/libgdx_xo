package com.amadare.xo_game.UI.effects;

import com.amadare.xo_game.UI.*;
import com.badlogic.gdx.math.Vector2;

public class MoveEffect extends UiEffect {
	float dX,dY;
	boolean mX,mY;
	Vector2 target;
	
	public MoveEffect(UiElement element, Vector2 target, float time){
		super(element);
		this.target=target;
		mX=target.x>element.Box.x;
		mY=target.y>element.Box.y;
		float aX=Math.abs(element.Box.x);
		float aY=Math.abs(element.Box.y);
		float atX=Math.abs(target.x);
		float atY=Math.abs(target.y);
		dX=atX>aX?atX-aX:aX-atX	/time*(mX?1:-1);
		dY=atY>aY?atY-aY:aY-atY	/time*(mY?1:-1);
		
		System.out.println(dX+" "+dY);
	}
	
	@Override
	public void Update(float delta) {
		super.Update(delta);
		
		element.Box.setX(element.Box.x+dX*delta);
		element.Box.setY(element.Box.y+dY*delta);
		
		if (target.x>element.Box.x && !mX){
			element.Box.setX(target.x);
			element.Box.setY(target.y);
			Enabled=false;
			System.out.println("end");
		}
	}
		
}
