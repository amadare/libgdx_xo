package com.amadare.xo_game.UI;

import java.util.*;

public class UiContainer {
	public List<UiElement> elements;
	UiElement hoveredElement;
	public boolean Enabled;	
	
	public UiContainer(){
		elements = new ArrayList<UiElement>();
		Enabled=true;
	}
	public void RemoveElement(UiElement element){
		if (hoveredElement==element){
			hoveredElement=null;
			elements.remove(element);
		}
		element.dispose();
	}
	public void Toggle(){
		Enabled=!Enabled;
	}
}
