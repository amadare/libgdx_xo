package com.amadare.xo_game.UI;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class UiLabel extends UiElement {
	public String Text;
	
	public UiLabel(String text, Rectangle rect){
		Text=text;
		Box=rect;
	}
	
	public void SetText(String text){
		Text=text;
	}

	@Override
	public void Draw(SpriteBatch batch, BitmapFont font) {
		if (color!=null){
			Color temp = font.getColor();
			font.setColor(color);
			font.draw(batch, Text, Box.x, Box.y);
			font.setColor(temp);
		}
		else
			font.draw(batch, Text, Box.x, Box.y);
		
	}

}
