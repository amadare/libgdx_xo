package com.amadare.xo_game.Screens;

import com.amadare.xo_game.*;
import com.amadare.xo_game.UI.*;
import com.amadare.xo_game.input.InputController;
import com.amadare.xo_game.players.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class GameScreen implements Screen {
	Xo game;
	UiContainerController uiController;
	InputController inputController;
	GameModel gameModel;
	GameController gameController;
	GameView gameView;
	BitmapFont bigFont;
	Player player1, player2;
	UiLabel nameLabel, winLabel1, winLabel2;

	public GameScreen(Xo game, int p1, int p2){
		this.game=game;
		gameModel=new GameModel();
		gameController = new GameController(gameModel);
		
		uiController=new UiContainerController();
		nameLabel = new UiLabel("",new Rectangle(12,222,0,0));
		loadContent();
		
		initPlayers(p1,p2);
		initGui();
		
		inputController = new InputController(game.camera);
		inputController.inputElements.add(uiController);
		gameView = new GameView(gameModel, game);	
	}
	
	private void initPlayers(int p1, int p2){
		if (p1!=0 && p2!=0){
			AiPlayer.Delay=.5f;
		} else{
			AiPlayer.Delay=0f;
		}
		switch (p1){
		case 0:
			player1= new Player(gameModel,gameController,1,true, "Player 1");
			break;
		case 1:
			player1=new StupidAiPlayer(gameModel,gameController,1){
				public void MakeTurn() {
					super.MakeTurn();
					addMark();
				}
			};
			break;
		}
		switch (p2){
		case 0:
			player2= new Player(gameModel,gameController,1,true, "Player 2");
			break;
		case 1:
			player2=new StupidAiPlayer(gameModel,gameController,2){
				public void MakeTurn() {
					super.MakeTurn();
					addMark();
				}
			};
			break;
		case 2:
			player2=new RandomAiPlayer(gameModel,gameController,2){
				public void MakeTurn() {
					super.MakeTurn();
					addMark();
				}
			};
			break;
		}
		
		nameLabel.SetText(player1.Name);
		if (!player1.IsHuman)
			player1.SetActive();
	}
	
	private void initGui(){
		//���� ���
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				final int x=i;
				final int y=j;
				uiController.AddElement("field",new CellButton(new Rectangle(7+i*64,7+j*64,64,64),game.getTexture("h")){
					public void Press(){
						if (gameController.place(x, y)){
							Active=false;
							addMark();
						}
					}
				});
			}
		}		
		uiController.SetContainerState("field", false);
		start();
		
		//�������� ��������� ������
		uiController.AddElement("menu", nameLabel);	
		
		uiController.AddElement("labels", new UiLabel(player1.Name,new Rectangle(256,150,0,0)));
		uiController.AddElement("labels", new UiLabel(player2.Name,new Rectangle(256,166,0,0)));		
		winLabel1 = new UiLabel("0",new Rectangle(256+5+game.font.getBounds(player1.Name).width,150,0,0));
		winLabel2 = new UiLabel("0",new Rectangle(256+5+game.font.getBounds(player2.Name).width,166,0,0));		
		uiController.AddElement("labels", winLabel1);
		uiController.AddElement("labels", winLabel2);
		
		//����
		uiController.AddElement("menu",new UiButton("Restart",new Rectangle(260,7,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				start();
			}
		});
		uiController.AddElement("menu",new UiButton("Exit",new Rectangle(260,73,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				game.setScreen(new MainMenuScreen(game));
			}
		});
	}
	
	private void start(){
		for (UiElement e: uiController.containers.get("field").elements)
			e.Active=true;
		gameController.clear();
		gameModel.xIsActive=player1.mark==1;
		if (player1.IsHuman)
			uiController.SetContainerState("field", true);
		else
			player1.SetActive();
	}
	
	private void addMark(){
		GameState state = gameController.getGameState();
		System.out.println(state.toString()+" "+player1.mark+" "+player2.mark+" "+gameModel.xIsActive);
		switch (state){
			case ActiveX:
				if (!player1.IsHuman){
					uiController.SetContainerState("field",false);
				}
				else
					uiController.SetContainerState("field",true);
				
				nameLabel.SetText(player1.Name);
				nameLabel.color=(player1.mark==2)?Color.RED:Color.BLUE;
				
				player1.SetActive();
				break;
			case ActiveO:
				if (!player2.IsHuman){
					uiController.SetContainerState("field",false);
				}
				else
					uiController.SetContainerState("field",true);
				nameLabel.SetText(player2.Name);
				nameLabel.color=(player2.mark==2)?Color.RED:Color.BLUE;
				
				player2.SetActive();
				break;
			case Draw:
				makeDraw();
			break;
			case WinnerX:
				makeWin(1);
			break;
			case WinnerO:
				makeWin(2);
			break;
		}		
	}
	
	private void makeDraw(){
		System.out.println("DRAW!");
		uiController.SetContainerState("field", false);
	}
	
	private void makeWin(int mark){
		if (mark==1)
			winLabel1.SetText(Integer.toString(player1.MakeWin()));
		else
			winLabel2.SetText(Integer.toString(player2.MakeWin()));
		
		uiController.SetContainerState("field", false);
		nameLabel.SetText(player1.Name);
		swap();
	}
	
	private void swap(){
		//TODO: ���������� �� �� ��� � ������
		/*int buffer = player1.mark;
		player1.mark=player2.mark;
		player2.mark=buffer;*/
	}
	
	private void loadContent() {
		game.loadTexture("back","images/big_back.png");
		game.loadTexture("x","images/x_.png");
		game.loadTexture("o","images/o_.png");
		game.loadTexture("h","images/h.png");
		game.loadTexture("block","images/block.png");
	}
	
	public void Update(float deltaTime){
		inputController.Update(deltaTime);
		player1.Update(deltaTime);
		player2.Update(deltaTime);		
	}
	
	public void Draw(float deltaTime){
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.camera.update();		
		
		game.batch.setProjectionMatrix(game.camera.combined);
		
		game.batch.begin();
			game.batch.draw(game.Textures.get("back"),0,0);
			game.batch.draw(game.Textures.get("block"),0,0);
			uiController.Draw(game.batch, game.font);
			gameView.Draw();		
		game.batch.end();
	}

	@Override
	public void render(float delta) {
		Update(delta);
		Draw(delta);
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
	

}
