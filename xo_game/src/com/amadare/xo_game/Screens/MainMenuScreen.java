package com.amadare.xo_game.Screens;
import com.amadare.xo_game.UI.*;
import com.amadare.xo_game.UI.effects.MoveEffect;
import com.amadare.xo_game.Xo;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.amadare.xo_game.input.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class MainMenuScreen implements Screen {
	final Xo game;
	UiController uiController;
	InputController inputController;
	
	
	public MainMenuScreen(Xo game){
		this.game=game;
		uiController=new UiController();
		loadContent();
		
		addMenuElements();
		
		inputController = new InputController(game.camera);
		inputController.inputElements.add(uiController);
	}
	
	public void addMenuElements(){
		final UiButton hvh = new UiButton("Human vs Human", 
				new Rectangle(7,10,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				game.setScreen(new GameScreen(game,0,0));
			}
		};
		final UiButton hvc = new UiButton("Human vs Comp", 
				new Rectangle(7,80,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				game.setScreen(new GameScreen(game,0,2));
			}
		};
		final UiButton cvc = new UiButton("Comp vs Comp", 
				new Rectangle(7,144,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				game.setScreen(new GameScreen(game,1,2));
			}
		};
		uiController.AddElement(hvh);
		uiController.AddElement(hvc);
		uiController.AddElement(cvc);

		/*uiController.AddElement(new UiButton("New game", 
				new Rectangle(10,10,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				System.out.println("new game!");
				//new MoveEffect(hvh, new Vector2(7,80),.7f);
				game.setScreen(new GameScreen(game,0,1));
			}
		});*/
		uiController.AddElement(new UiButton("Exit", 
				new Rectangle(138,10,128,64),
				game.getTexture("b_normal"),
				game.getTexture("b_pressed")){
			public void Press(){
				super.Press();
				System.out.println("Exit!");
				Gdx.app.exit();
				//new MoveEffect(this, new Vector2(0,0), 2f);
			}
		});
		
		
	}
	
	void loadContent(){
		game.loadTexture("back","images/big_back.png");
		game.loadTexture("b_normal", "images/button.png");
		game.loadTexture("b_pressed", "images/button_pressed.png");
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.camera.update();
		
		inputController.Update(delta);
		uiController.Update(delta);
		
		game.batch.setProjectionMatrix(game.camera.combined);
		
		game.batch.begin();
			game.batch.draw(game.Textures.get("back"),0,0);
			uiController.Draw(game.batch, game.font);
		game.batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		System.out.println("main Disposed");
		game.Textures.remove("back");
	}

}
