package com.amadare.xo_game.input;

import java.util.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class InputController {
	private static boolean prevButtonState;
	OrthographicCamera camera;
	
	public List<NeedInput> inputElements;
	
	//TODO: ϳ������� ���� ������ ������
	public InputController(OrthographicCamera camera){
		prevButtonState=false;
		inputElements=new ArrayList<NeedInput>();
		this.camera=camera;
	}
	
	public void Update(float deltaTime){
		boolean buttonState=Gdx.input.isButtonPressed(0)||Gdx.input.isTouched();
		Vector3 touchPos = new Vector3();
        touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(touchPos);
			for (NeedInput i: inputElements)
				i.handleInput(touchPos.x, touchPos.y, !buttonState && prevButtonState);		
		prevButtonState=buttonState;
	}
}


