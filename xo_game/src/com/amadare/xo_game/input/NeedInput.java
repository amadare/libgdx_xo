package com.amadare.xo_game.input;

public interface NeedInput{
	public void handleInput(float x, float y, boolean pressed);
}
