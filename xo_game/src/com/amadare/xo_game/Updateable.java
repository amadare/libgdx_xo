package com.amadare.xo_game;

public interface Updateable {
	public void Update(float delta);
}
