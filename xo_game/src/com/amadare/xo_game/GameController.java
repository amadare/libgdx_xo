package com.amadare.xo_game;

import com.amadare.xo_game.players.Player;


public class GameController {	
	private GameModel game;
	
	public GameController(GameModel game){
		this.game=game;
	}

	
	public boolean place(int x, int y){
		if (game.map[x][y]!=0)
			return false;
		
		game.map[x][y]=game.xIsActive?1:2;
		game.xIsActive=!game.xIsActive;
		
		return true;
	}	
	
	public GameState getGameState(){
		marks:
		for (int h=1; h<3; h++){
			cols:
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						//->
						if (game.map[j][i]!=h)
							continue cols;
					}
					return h==1?GameState.WinnerX:GameState.WinnerO;
				}
		
			rows:
				for (int i=0; i<3; i++){
					for (int j=0; j<3; j++){
						// \/\/
						if (game.map[i][j]!=h)
							continue rows;
					}
					return h==1?GameState.WinnerX:GameState.WinnerO;
				}
				
			boolean win=true;			
			diag:
				for (int i=0; i<3; i++)
					if (game.map[i][i]!=h){
						win=false;
						break;
					}
			if (win)
				return h==1?GameState.WinnerX:GameState.WinnerO;
			win=true;
			diags2:
				for (int i=0; i<3; i++){
					if (game.map[i][2-i]!=h){
						win=false;
						break;
					}
				}
			if (win)
				return h==1?GameState.WinnerX:GameState.WinnerO;
		}
		
		boolean draw=true;
		drawCheck:
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				if (game.map[i][j]==0){
					draw=false;
					break drawCheck;
				}
					
		return draw?GameState.Draw:game.xIsActive?GameState.ActiveX:GameState.ActiveO;
	}
	
public void clear(){
		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				game.map[i][j]=0;
		
		//game.xIsActive=true;
	}
}
