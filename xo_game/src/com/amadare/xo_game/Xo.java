package com.amadare.xo_game;

import java.util.*;

import com.amadare.xo_game.Screens.MainMenuScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;

public class Xo extends Game {
	
	public SpriteBatch batch;
	public OrthographicCamera camera;
	public BitmapFont font;
	public Map<String,Texture> Textures;

	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		batch = new SpriteBatch();
		font=new BitmapFont(true);
		font.setColor(Color.BLUE);
		Textures = new HashMap<String, Texture>();
		
		camera = new OrthographicCamera();
		camera.setToOrtho(true,400,240);
		
		this.setScreen(new MainMenuScreen(this));
	}
	
	public void loadTexture(String name, String path){
		if (!Textures.containsKey(name)){
			Textures.put(name, new Texture(Gdx.files.internal(path)));
		}
	}
	public Texture getTexture(String name){
		return Textures.get(name);
	}
	
	public void render() {
        super.render();
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
    }

}


//TODO: ������� ���� �������
