package com.amadare.xo_game;

public enum GameState{
	Draw,
	WinnerO,
	WinnerX,
	ActiveX,
	ActiveO
}
