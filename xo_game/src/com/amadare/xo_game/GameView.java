package com.amadare.xo_game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameView {
	GameModel model;
	Xo game;
	
	public GameView(GameModel model, Xo game){
		this.model=model;
		this.game=game;
	}
	public void Draw(){
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				if (model.map[i][j]==0)
					continue;
					game.batch.draw(game.getTexture(model.map[i][j]==1?"x":"o"),7+64*i,7+64*j,64,64);
			}
		}
	}
}
