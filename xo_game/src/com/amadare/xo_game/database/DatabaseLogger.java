package com.amadare.xo_game.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseLogger {
	private Connection bd;
	private Statement st;
	private ResultSet rs;
	
	public DatabaseLogger() {
		
	}
	
	public static Connection Connect(){
		Connection con;
		try {
			Class.forName("org.sqlit.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:D:\\testdb.db");
			return con;
		} catch (ClassNotFoundException | SQLException e) {
			return null;
		}		
	}
	
	public void main(String[] args){
		Connection s = Connect();
		
	}
}
